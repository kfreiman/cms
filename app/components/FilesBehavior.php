<?php

namespace app\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\web\UploadedFile;
use himiklab\thumbnail\EasyThumbnailImage;


class FilesBehavior extends Behavior
{
    public $filesAttributes;

    public $multiple = false;

    public $fileModelClass = 'app\models\File';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
        ];
    }

    public function __get($name)
    {
        if (in_array($name, $this->filesAttributes))
        {
            return $this->getFiles($name);
        }
        else
        {
            return parent::__get($name);
        }
    }

    public function canGetProperty($name, $checkVars = true)
    {
        if (in_array($name, $this->filesAttributes))
        {
            return true;
        }
        else
        {
            return parent::canGetProperty($name, $checkVars);
        }
    }

    protected function getFiles($attribute)
    {
        $class = $this->fileModelClass;
        $query = $class::find();
        $query->primaryModel = $this->owner;
        $query->link = [
            'related_id' => 'id',
        ];
        $query->andWhere(['related_class' => get_class($this->owner)])
              ->andWhere(['related_attribute' => $attribute]);
        return $this->multiple ? $query->all() : $query->one();
    }

    protected function createAndSaveUploadedFiles($attribute)
    {
        $class = $this->fileModelClass;
        $uploadedFiles = UploadedFile::getInstances($this->owner, $attribute);
        foreach($uploadedFiles as $uploadedFile)
        {
            $class::makeByUploadedFile(
                $uploadedFile,
                $this->owner,
                $attribute
            );
        }
    }

    public function afterUpdate($event)
    {
        foreach($this->filesAttributes as $attribute)
        {
            $this->createAndSaveUploadedFiles($attribute);
        }
    }




}
