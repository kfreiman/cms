<?php

namespace app\components;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use app\models\Img;
use Yii;
use himiklab\thumbnail\FileNotFoundException;
use himiklab\thumbnail\EasyThumbnailImage;
use yii\web\UploadedFile;


class ImgBehavior extends Behavior
{

    public $width;
    public $height;
    public $defaultThumbPath;

    public function getThumbUrl()
    {
        $filePath = $this->owner->filePath;

        if (!is_file($filePath)) {
            $filePath = $this->defaultThumbPath;
        }

        // var_dump($filePath);
        if (!is_file($filePath)) {
            return false;
        }

        $w = $this->width ?: 100;
        $h = $this->height ?: 100;

        return EasyThumbnailImage::thumbnailFileUrl(
            $filePath,
            $w,
            $h,
            EasyThumbnailImage::THUMBNAIL_OUTBOUND
        );
    }


}
