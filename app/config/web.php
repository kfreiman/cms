<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'bootstrap' => ['log', 'thumbnail'],
    'modules' => [
        'settings' => [
            'class' => 'pheme\settings\Module',
        ],
    ],
    'components' => [
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'my_application_cart',
        ],
        'settings' => [
            'class' => 'pheme\settings\components\Settings'
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'M5V_78ZLy0GTP0qwI15LhY3f9nGzkjR4',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/gallery_thumbnails',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'login' => 'site/login',
                'logout' => 'site/logout',
                // '<controller:(post|comment)>/<id:\d+>/<action:(create|update|delete)>' => '<controller>/<action>',
                // '<controller:(post|comment)>/<id:\d+>' => '<controller>/view',
                // '<controller:(post|comment)>s' => '<controller>/index',
            ],
        ],
        'formatter' => [
            'timeZone' => 'Europe/Moscow',
            'dateFormat' => 'd.MM.Y',
            'timeFormat' => 'H:mm:ss',
            'datetimeFormat' => 'd.MM.Y H:mm',
        ],
        'i18n' => [
            'translations' => [
                // '*' => [
                //     'class' => 'yii\i18n\PhpMessageSource',
                //     'basePath' => '@app/messages',
                //     'fileMap' => [
                //         'app' => 'app.php',
                //     ]
                // ],
                'extensions/yii2-settings/*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'extensions/yii2-settings/settings' => 'settings.php',
                    ],
                ]

            ],
        ],
        // 'view' => [
            // 'renderers' => [
            //     'handlebars' => [
            //         'class' => 'exertis\handlebars\ViewRenderer',
            //         // the file extension of Handlebars templates
            //         // 'extension' => '.handlebars',
            //         // path alias pointing to where Handlebars cache will be stored. Set to false to disable templates cache.
            //         // 'cache' => '@app/runtime/handlebars',
            //         // array helpers to preload, can contain class names (strings).
            //         // If empty - only default helpers will be preloaded
            //         // 'helpers' => [],
            //         // a callable function to escape values
            //         // 'escape' => 'htmlspecialchars',
            //         // array to pass as extra parameter to the escape function
            //         // 'escapeArgs' => [ENT_COMPAT, 'UTF-8']
            //     ],
            // ],
        // ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
