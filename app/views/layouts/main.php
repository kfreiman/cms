<?php
use app\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php echo Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>
<?php if (!\Yii::$app->user->isGuest){


    NavBar::begin();
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],

            'items' => [
                [
                    'label' => 'Главная',
                    'url' => '/group/index',
                ],
                [
                    'label' => 'Группы',
                    'url' => '/group/admin-index',
                ],
                [
                    'label' => 'Продукты',
                    'url' => '/product/admin-index',
                ],
                [
                    'label' => 'Заказы',
                    'url' => '/order/index',
                ],
                [
                    'label' => 'Admin',
                    'items' => [
                        [
                            'label' => 'Настройки',
                            'url' => '/config',
                        ],
                        [
                            'label' => 'Выйти',
                            'url' => '/logout',
                        ],
                    ],
                ],
            ],
        ]);

    NavBar::end();


} ?>


<!-- <div class="abc">
<style scoped>
    .abc {color: red;}
</style>
    <div class="abc">in scope</div>
asfefaefaefeaf
</div> -->


<?php $this->beginBody() ?>

<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
