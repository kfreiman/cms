<?php
use yii\helpers\Html;
use himiklab\thumbnail\EasyThumbnailImage;



?>
<div class="row">
    <div class="col-md-6">
        <?php echo Html::img($model->thumbUrl)?>
    </div>
    <div class="col-md-6">
        <div>
        <?= $product->main_img_id == $model->id ?
            '<b>Главное изображение</b>' :
            Html::a(
                'Cделать главным',
                ['/product/main-img', 'product_id'=>$product->id, 'img_id'=>$model->id]
            )
        ?>
        </div>
        <div>
            <?= Html::a('Удалить', ['/file/delete', 'id'=>$model->id], ['data-confirm' => 'Действиетльно удалить изображение?',])?>
        </div>

    </div>
</div>
<hr>
