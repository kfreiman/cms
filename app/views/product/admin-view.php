<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('К списку продуктов', ['admin-index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-link pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'group.name',
            'name',
            'description:ntext',
            'price',
        ],
    ]) ?>

    <h3>Изображения</h3>
    <?php

    var_dump($model->mainThumb->id);

    echo ListView::widget( [
        'dataProvider' => $model->imgsDataProvider,
        'itemView' => '_image',
        'viewParams' => ['product'=>$model],
    ]) ?>


    <?php
    // var_dump($model->images);
    // foreach ($model->image as $image) {
    //     var_dump($image->delete());
    // }
     ?>
    <?= Html::a('Добавить изображения', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>


</div>
