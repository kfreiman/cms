<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Просмотр', ['/group/index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Создать продукт', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'group.name',
                'name',
                'description',
                'price',


                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-eye-open"></span>',
                                ['/product/view', 'id'=>$model->id],
                                [
                                    'title' => Yii::t('yii', 'View'),
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ]
                ],
            ],
        ]); ?>

</div>
