({
    block: 'main',
    content: [
        {
            block : "link-to-cart",
            tag : 'a',
            cls : "btn btn-success btn-sm",
            attrs : {href:'/site/cart'},
            content : 'Перейти в корзину',
        },
        {
            tag : 'h1',
            content : 'Список продуктов',
        },
        {
            block: "product-list",
            content: [
                <?php foreach ($products as $product) { ?>
                    {
                        block : 'product',

                        content : [
                            {
                                elem : 'title',
                                content : '<?= $product->name ?>'
                            },
                            {
                                elem : 'price',
                                content : 'цена: <?= $product->price ?>'
                            },
                            {
                                elem : 'description',
                                content : '<?= $product->description ?>'
                            },
                            {
                                block : 'add-to-cart',
                                tag : 'button',
                                cls : 'btn btn-primary btn-sm',
                                content : 'Добавить в корзину',
                                js : {
                                    product_id : <?= $product->id ?>
                                }
                            },
                            {
                                tag : 'hr',
                            },
                        ],
                    },
                <?php } ?>
            ],
        },

        {
            block : 'cart-add-modal',
            body_text : 'f',
            buttons : {
                "dismiss" : {
                    block : 'to-cart-button',
                    cls : 'btn btn-sm btn-default',
                    attrs : {'data-dismiss' : 'modal'},
                    content : 'Продолжить',
                },
                "to-cart" : true,
            }
        }

    ],
})

