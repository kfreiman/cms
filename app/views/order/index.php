<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <div class="order-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'create_time',
                // 'update_time',
                'status',
                'name',
                'phone',
                'email:email',
                'totalCost',
                // 'mode_delivery',
                // 'mode_payment',
                // 'user_comment:ntext',
                // 'admin_comment:ntext',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a(
                                '<span class="glyphicon glyphicon-eye-open"></span>',
                                ['/order/admin-view', 'id'=>$model->id],
                                [
                                    'title' => Yii::t('yii', 'View'),
                                    'data-pjax' => '0',
                                ]
                            );
                        },
                    ]
                ],
            ],
        ]); ?>

    </div>
</div>
