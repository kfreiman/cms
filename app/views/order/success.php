{
    block : 'order-success',
    content : [
        {
            block : 'title',
            tag : 'h2',
            content : 'Ваш заказ успешно создан!'
        },
        {
            block : 'description',
            tag : 'p',
            content : 'Вскоре мы с вами свяжемся! Номер вашего заказа #<?= $model->id ?>'
        },

    ]
}
