<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <?= Html::a('К списку заказов', ['/order/index']) ?>

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'create_time',
            'update_time',
            'status',
            'name',
            'phone',
            'email:email',
            'address:ntext',
            'mode_delivery',
            'mode_payment',
            'user_comment:ntext',
            'admin_comment:ntext',
            'totalCost',
            'count',
            'totalCount',
        ],
    ]) ?>

    <hr>
    <h3>Заказано</h3>
    <?= GridView::widget([
        'dataProvider' => $model->itemsProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label'=>'Название',
                'format'=>'html',
                'value'=>function ($data) {
                    return Html::a($data->product->name, ['/product/view', 'id'=>$data->product->id]);
                },
            ],
            'product.price',
            'count',
            'cost',
        ],
    ]); ?>


</div>
