<?php

// var_dump($model->errors);
?>

{
    block : 'order-create',
    content : [
        {
            block : 'title',
            content : '<h3>Оформление заказа</h3>'
        },
        {
            block : 'order-form',
            tag : 'form',
            attrs : {action : '', method : 'post'},
            content : [
                {
                    elem : 'error-summary',
                    content : [
                        {
                            elem : 'error-explain',
                            content : 'Необходимо исправить следующие ошибки'
                        },
                        {
                            elem : 'errors-list',
                            tag : 'ol',
                            content : [
                                <?php foreach ($model->errors as $error) {?>
                                    {
                                        elem : 'error-message',
                                        tag : 'li',
                                        content : '<?= implode(', ', $error) ?>',
                                    },
                                <?php } ?>
                            ]
                        }
                    ]
                },
                {
                    block : 'order-input-name',
                    content : [
                        {
                            elem : 'label-name',
                            tag : 'label',
                            attrs : {for : 'order-name'},
                            content : 'Имя'
                        },
                        {
                            elem : 'input-name',
                            tag : 'input',
                            attrs : {
                                id : 'order-name',
                                name : 'Order[name]',
                                type : 'text',
                                value : '<?= $model->name ?>',
                            },
                        }
                    ]
                },
                {
                    block : 'order-input-phone',
                    content : [
                        {
                            elem : 'label-phone',
                            tag : 'label',
                            attrs : {for : 'order-phone'},
                            content : 'Телефон'
                        },
                        {
                            elem : 'input-phone',
                            tag : 'input',
                            attrs : {
                                id : 'order-phone',
                                name : 'Order[phone]',
                                type : 'text',
                                value : '<?= $model->phone ?>',
                            }
                        }
                    ]
                },
                {
                    block : 'order-input-email',
                    content : [
                        {
                            elem : 'label-email',
                            tag : 'label',
                            attrs : {for : 'order-email'},
                            content : 'Email'
                        },
                        {
                            elem : 'input-name',
                            tag : 'input',
                            attrs : {
                                id : 'order-email',
                                name : 'Order[email]',
                                type : 'text',
                                value : '<?= $model->email ?>',
                            },
                        }
                    ]
                },
                {
                    block : 'order-form-submit',
                    content : [
                        {
                            elem : 'button-submit',
                            cls : 'btn btn-success',
                            tag : 'button',
                            attrs : {type : 'submit', name : '_csrf', value : '<?=Yii::$app->request->getCsrfToken()?>'},
                            content : 'Отправить',
                        }
                    ]
                },
                {
                    block : 'csrf-input',
                    content : [
                        {
                            elem : 'input-csrf',
                            tag : 'input',
                            attrs : {type : 'hidden', name : '_csrf', value : '<?=Yii::$app->request->getCsrfToken()?>'},
                        }
                    ]
                }

            ]
        }
    ]
}
