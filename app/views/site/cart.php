{
    block : 'cart',
    cls: 'container',
    content : [
        {
            block : 'cart-header',
            tag : 'h1',
            content : 'Корзина',
        },
        {
            block: "cart-list",
            content: [
                <?php foreach ($positions as $position) { ?>
                    {
                        block : 'product',

                        content : [
                            {
                                elem : 'title',
                                content : 'Наименование продукта: <?= $position->name ?>'
                            },
                            {
                                elem : 'price',
                                content : 'Цена: <?= $position->price ?>'
                            },
                            {
                                elem : 'description',
                                content : '<?= $position->description ?>'
                            },
                            {
                                elem : 'description',
                                content : 'Количество в корзине: <?= $position->quantity ?>'
                            },
                            {
                                elem : 'position-cost',
                                content : 'Общая стоимость: <?= $position->cost ?>'
                            },
                            {
                                tag : 'hr',
                            },
                        ],
                    },
                <?php } ?>
            ],
        },
        {
            block : 'cart-cost',
            content : 'Общая стоимость: <?= $cart->cost ?>'
        },
        {
            tag : 'hr',
        },
        {
            block : 'link-to-order',
            tag : 'a',
            attrs : {href : '/order/create'},
            cls : 'btn btn-primary',
            content : 'Оформить заказ'
        }
    ],
}
