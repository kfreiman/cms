({
    block: 'page',
    cls: 'container',
    content: [
        {
            block : 'navigation',
            content : [
                <?php foreach ($menu as $menu_item) { ?>
                    {
                        block: 'menu-item',
                        mods : {
                            active : '<?= $menu_item['active'] ? 'yes' : 'not'?>',
                        }
                        content : {
                            block : 'link',
                            tag : 'a',
                            attrs : {
                                href : '<?= $menu_item['url']?>'
                            },
                            content : '<?= $menu_item['title']?>'
                        },
                    },
                <?php } ?>

            ]
        },
        {
            block: "main",
            content: "Содержание основного блока",
        }
    ],
})
