<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Group;
// use app\models\Img;
use kartik\file\FileInput;
use himiklab\thumbnail\EasyThumbnailImage;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(
        $model->isNewRecord ?
            ArrayHelper::map(Group::find()->all(), 'id', 'name') :
            ArrayHelper::map(Group::find()->where('id != :id', [':id'=>$model->id])->all(), 'id', 'name'),
        ['prompt'=>'(не задано)']
    ) ?>

    <?= $form->field($model, 'type')->dropDownList($model->typeList ) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?php
        echo Html::img($model->thumbUrl, ['alt'=>'Миниатюра']);
    ?>

    <?= $form->field($model, 'image[]')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions' => [
            'showCaption' => false,
            'showRemove' => false,
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  $model->isNewRecord ? 'Заменить обложку' : 'Выбрать обложку'
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
