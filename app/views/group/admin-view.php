<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Group */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('К списку групп', ['admin-index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-link pull-right',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'typeText',
            'name',
            [
                'attribute'=>'parent_id' ,
                'format'=>'html',
                'value'=> $model->parent ?
                    Html::a($model->parent->name,
                        ['/group/admin-view', 'id'=>$model->parent->id]
                ) :
                    '(не задана)',

            ],
            'description:ntext',
            [
                'attribute'=>'image' ,
                'format'=>'html',
                'value'=> Html::img($model->thumbUrl)

            ],
        ],
    ]) ?>

<?php

// echo Html::img($model->thumbUrl);
// var_dump($model->filePath);

// echo Html::img($model->thumbUrl, ['alt'=>'abc']);
?>
</div>
