({
    block: 'main',
    cls: 'container',
    content: [
        {
            tag : 'h1',
            content : 'Список групп',
        },
        {
            block: "product-list",
            content: [
                <?php foreach ($groups as $group) { ?>
                    {
                        block : 'product-item',

                        content : [
                            {
                                block : 'link',
                                url : '<?= $group->url ?>',
                                content : '<?= $group->name ?>'
                            },
                            {
                                tag : 'br',
                            },
                            {
                                block : 'image',
                                url : '<?= $group->thumbUrl ?>',
                                title : 'Все подробности на bem.info'
                            },
                            {
                                tag : 'hr',
                            },
                        ],
                    },
                <?php } ?>
            ],
        },



    ],
})

