<?php

namespace app\controllers;

use Yii;
use app\models\Group;
use app\models\Thumb;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\bem\BemController;
use yii\web\UploadedFile;
use himiklab\thumbnail\EasyThumbnailImage;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends BemController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin-view', 'admin-index', 'update', 'delete', 'create'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex()
    {
        $groups = Group::find()->where('parent_id IS NULL')->all();

        return $this->renderBemjson('index.handlebars', ['groups'=>$groups]);
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionAdminIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Group::find(),
        ]);

        return $this->render('admin-index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $group = $this->findModel($id);

        // var_dump(is_array($group->products[0]));
        if ($group->type == Group::TYPE_PRODUCT) {
            return $this->renderBemjson('view_product.handlebars', [
                'group' => $group,
            ]);
        } else if ($group->type == Group::TYPE_GROUP) {
            return $this->renderBemjson('view_group.handlebars', [
                'group' => $group,
            ]);
        }

    }

        /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionAdminView($id)
    {

        return $this->render('admin-view', [
            'model' => $this->findModel($id),
        ]);
    }


    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Group();
        // $model->thumb_id = $this->saveThumb($model, 'create');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['admin-view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            return $this->redirect(['admin-view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin-index']);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



}
