<?php

namespace app\controllers;

use Yii;
use app\models\Product;
use app\bem\BemController;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yz\shoppingcart\ShoppingCart;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use app\models\ProductImg;
use app\models\Img;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends BemController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['admin-index', 'update', 'delete', 'create', 'admin-view'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $products = Product::find()->all();

        return $this->renderBemjson('index', ['products'=>$products]);
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionAdminIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
        ]);

        return $this->render('admin-index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {

        return $this->renderBemjson('view.handlebars', ['product' => $this->findModel($id)]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionAdminView($id)
    {
        return $this->render('admin-view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['admin-view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post()) && $model->save()) {



            return $this->redirect(['admin-view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['admin-index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Добавляет позицию в корзину
     * @param  integer $id - id продукта
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddToCart($id, $count=1)
    {
        $cart = Yii::$app->cart;

        $model = $this->findModel($id);

        $cart->put($model, $count);

        // если аякс - говорю, что получилось
        if (Yii::$app->request->isAjax) {
            echo '{result:"ok"}';
            Yii::$app->end();
        } else {
            return $this->redirect(['cart-view']);
        }
    }

    /**
     * Делает изображение главным
     * @param  integer $product_id - id продукта
     * @param  integer $img_id - id файла
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionMainImg($product_id, $img_id)
    {
        $model = $this->findModel($product_id);

        // проверяю, что id присланного изображение в числе тех, которые закреплены
        foreach ($model->images as $image) {
            if ($image->id == $img_id) {
                $model->main_img_id = $img_id;
                $model->save(false);
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return 'Не удалось сделать изображение главным';
    }
}
