<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Product;
use app\models\ContactForm;
use app\bh\Template;
use app\bem\BemController;

use Handlebars\Handlebars;

class SiteController extends BemController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->redirect(['/group/index']);
    }

    public function actionTest()
    {
        $product = Product::findOne(1);
        return  $this->renderBemjson('test.handlebars', [
            'test'=>false,
            'product'=>$product
        ]);

    }

    public function actionContact()
    {
        return 'Страница с контактами';
    }



    public function actionAbout()
    {
        return 'Страница о компании';
    }


    public function actionCart()
    {
        $cart = Yii::$app->cart;
        $positions = $cart->getPositions();

        return $this->renderBemjson('cart', ['positions'=>$positions, 'cart'=>$cart]);
    }


     public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


}
