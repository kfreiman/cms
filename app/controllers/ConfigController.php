<?php

namespace app\controllers;

use Yii;
use pheme\settings\controllers\DefaultController as DefaultController;
use yii\filters\AccessControl;

class ConfigController extends DefaultController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }





}
