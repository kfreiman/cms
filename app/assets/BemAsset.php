<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * This asset bundle provides the base javascript files for the Yii Framework.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class BemAsset extends AssetBundle
{
     public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/i-bem.js',
        // 'js/i-bem-browser.js',
    ];
    // public $sourcePath = '@bower/i-bem';
    // public $js = [
    //     'i-bem.js',
    // ];
}
