<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use \Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    // public $js = [
    //     'js/scoped.js'
    // ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
    public $depends = [
        'yii\web\YiiAsset',
        // 'app\assets\Theme1Asset',
        // 'app\assets\MyBootstrapAsset',
    ];

    public function init()
    {
        // прикрепляю все css файлы блоков
        $this->collectCss();

        // прикрепляю все js файлы блоков
        // $this->collectJs();
    }


    protected function collectCss()
    {
        $webroot = \Yii::getAlias('@webroot');
        $block_css = glob($webroot. '/blocks/**/*.css');
        $elem_and_mod_css = glob($webroot. '/blocks/**/**/*.css');
        $all_css = array_merge($block_css, $elem_and_mod_css);

        foreach ($all_css as $css_file) {
            $this->css[] = str_replace($webroot.'/', '', $css_file);
        }
    }

    protected function collectJs()
    {
        $webroot = \Yii::getAlias('@webroot');
        $block_js = glob($webroot. '/blocks/**/*.js');
        $elem_and_mod_js = glob($webroot. '/blocks/**/**/*.js');
        $all_js = array_merge($block_js, $elem_and_mod_js);

        foreach ($all_js as $js_file) {
            $this->js[] = str_replace($webroot.'/', '', $js_file);
        }
    }

}
