<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * This asset bundle provides the base javascript files for the Yii Framework.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Theme1Asset extends AssetBundle
{
    public $basePath = '@webroot/theme_1';
    public $baseUrl = '@web/theme_1';

    public $js = [
        // 'js/jquery/latest/jquery-latest.min.js',
        // 'bootstrap/js/bootstrap.min.js',
        'js/jquery.cycle2.min.js',
        'js/jquery.easing.1.3.js',
        'js/jquery.parallax-1.1.js',
        'js/helper-plugins/jquery.mousewheel.min.js',
        'js/smoothproducts.js',
        'js/jquery.mCustomScrollbar.js',
        'js/ion-checkRadio/ion.checkRadio.min.js',
        'js/grids.js',
        'js/owl.carousel.min.js',
        'js/jquery.minimalect.min.js',
        'js/bootstrap.touchspin.js',
        'js/script.js',
    ];

    public $css = [
        'bootstrap/css/bootstrap.css',
        'css/style.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/ion.checkRadio.css',
        'css/ion.checkRadio.cloudy.css',
        'css/jquery.mCustomScrollbar.css',
        'css/smoothproducts.css',
    ];
}
