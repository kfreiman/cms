<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * File
 * @package app\models
 *
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%file}}';
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'size', 'type', 'related_class', 'related_attribute'], 'filter', 'filter' => 'trim'],
            [['name', 'size', 'type', 'related_id', 'related_class', 'related_attribute'], 'required'],
            [['related_id', 'size'], 'number', 'integerOnly' => true],
            [['type', 'related_class', 'related_attribute'], 'string'],
        ];
    }

    public function init()
    {
        parent::init();
        $dirPath = $this->getDirPath();
        if (!is_dir($dirPath)) {
            FileHelper::createDirectory($dirPath);
        }
    }

    protected function getDirPath()
    {
        return \Yii::getAlias('@webroot/uploads');
    }

    public function getFilePath()
    {
        return $this->getDirPath() . DIRECTORY_SEPARATOR . $this->id . '_' . $this->name;
    }

    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);
        if ($insert)
        {
            $ext = end((explode(".", $this->name)));
            $this->name_hash = \Yii::$app->security->generateRandomString() . ".{$ext}";
        }
        return $return;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete())
        {
            return unlink($this->getFilePath());
        }
        else
        {
            return false;
        }

    }

    public static function findByNameHash($nameHash)
    {
        return static::find()->andWhere(['name_hash' => $nameHash])->one();
    }

    /**
     * Creates a [[\app\models\File]] based on the [[yii\web\UploadedFile]]
     * @param UploadedFile $uploadedFile
     * @param $relatedModel
     * @param $relatedAttribute
     * @return bool
     */
    public static function makeByUploadedFile(UploadedFile $uploadedFile, $relatedModel, $relatedAttribute)
    {
        $file = new static();
        $file->name = $uploadedFile->name;
        $file->size = $uploadedFile->size;
        $file->type = $uploadedFile->type;
        $file->related_id = $relatedModel->id;
        $file->related_class = get_class($relatedModel);
        $file->related_attribute = $relatedAttribute;
        $saved = $file->save();
        return $saved && $uploadedFile->saveAs($file->getFilePath());
    }


}
