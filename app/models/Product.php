<?php

namespace app\models;

use Yii;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
use app\models\Group;
use app\models\ProductImg;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * This is the model class for table "cms_product".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $price
 *
 * @property Item[] $items
 */
class Product extends \yii\db\ActiveRecord implements CartPositionInterface
{
    use CartPositionTrait;

    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }


    public function behaviors()
    {
        return [
            'uploadFile' => [
                'class' => \app\components\FilesBehavior::className(),
                'filesAttributes' => ['images'],
                'multiple' => true,
                'fileModelClass' => 'app\models\ProductImg'
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'price'], 'required'],
            [['description'], 'string'],
            [['price', 'group_id'], 'number'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_id' => 'Родительская группа',
            'name' => 'Название',
            'description' => 'Описание',
            'price' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['product_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUrl()
    {
        return Url::to(['/product/view', 'id'=>$this->id]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    public function beforeSave()
    {
        // если эта группа прикрепляется к другой - выставляю у родительской тип "папка"
        if ($this->group) {
            $parent = $this->group;
            $parent->type = Group::TYPE_PRODUCT;
            if ($parent->save()) {
                return true;
            } else {
                $this->addError(
                    'parent_id',
                    'У родительской группы не удалось установить тип '.
                        Group::typeText(Group::TYPE_PRODUCT) . ' ('.$parent->errors['type'][0].')');

                return false;
            }
        }

        return true;
    }


    public function getImgsDataProvider()
    {
        return $provider = new ActiveDataProvider([
            'query' => ProductImg::find()->where(['related_id' => $this->id])
                ->andWhere(['related_class' => self::className()])
                ->andWhere(['related_attribute' => 'images']) ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
    }


    public function getMainThumb()
    {
        if ($this->main_img_id) {
            return ProductImg::findOne($this->main_img_id);
        } else {
            return null;
        }
    }
}
