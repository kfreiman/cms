<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use app\components\ImgBehavior;

use himiklab\thumbnail\EasyThumbnailImage;
/**
 * This is the model class for table "cms_group".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $type
 * @property string $name
 * @property string $description
 *
 * @property Group $parent
 * @property Group[] $groups
 * @property Product[] $products
 */
class Group extends \yii\db\ActiveRecord
{
    const TYPE_PRODUCT = 0;
    const TYPE_GROUP = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_group';
    }

    public function behaviors()
    {
        return [
            'uploadFile' => [
                'class' => \app\components\FilesBehavior::className(),
                'filesAttributes' => ['image'],
                'multiple' => false
            ],
            'imgBehavior' => [
                'class' => \app\components\ImgBehavior::className(),
                'width' => Yii::$app->settings->get('groups.width'),
                'height' => Yii::$app->settings->get('groups.height'),
                'defaultThumbPath' => Yii::getAlias('@webroot') .'/images/default_group.png',
            ]
        ];
    }

    public function getFilePath()
    {
        return $this->image ? $this->image->filePath : false;
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'type'], 'integer'],
            [['name'], 'required'],
            [['type'], 'canBeThisType'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    public function canBeThisType($attr, $params)
    {
        $reason = false;

        // к группе уже прикреплены продукты => её нельзя сделать папкой групп
        if ($this->$attr == self::TYPE_GROUP && count($this->products)) {
            $reason = 'к группе уже прикреплены продукты';
        } else if ($this->$attr == self::TYPE_PRODUCT && count($this->groups)) {
            $reason = 'к группе уже прикреплены подгруппы';
        }

        if ($reason) {
            $this->addError(
                $attr,
                'Группа не может быть типа "'.$this->typeText.'", так как '.$reason
            );
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительская группа',
            'type' => 'Тип',
            'name' => 'Группа',
            'description' => 'Описание',
            'typeText' => 'Тип группы',
            // 'img_id' => 'Миниатюра',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Group::className(), ['id' => 'parent_id']);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['group_id' => 'id']);
    }

    public function getTypeText()
    {
        return $this->typeList[$this->type] ?: '(тип не определён)';
    }

    public static function typeText($type=self::TYPE_PRODUCT)
    {
        return self::typeList()[$type];
    }

    public function getTypeList()
    {
        return self::typeList();
    }

    public static function typeList()
    {
        return [
            self::TYPE_PRODUCT => 'Каталог товаров',
            self::TYPE_GROUP => 'Папка'
        ];
    }

    public function getUrl()
    {
        return Url::to(['/group/view/', 'id'=>$this->id]);
    }



    public function beforeSave()
    {
        // если эта группа прикрепляется к другой - выставляю у родительской тип "папка"
        if ($this->parent) {
            $parent = $this->parent;
            $parent->type = self::TYPE_GROUP;
            if ($parent->save()) {
                return true;
            } else {
                // если не получилось выставить - добавляю в список ошибок причину
                $this->addError(
                    'parent_id',
                    'У родительской группы не удалось установить тип '.
                        self::typeText(self::TYPE_GROUP) . ' ('.$parent->errors['type'][0].')');

                return false;
            }
        }

        return true;
    }




}
