<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
use himiklab\thumbnail\EasyThumbnailImage;

/**
 * This is the model class for table "cms_img".
 *
 * @property integer $id
 * @property string $name
 * @property string $ext
 * @property string $src
 * @property string $alt
 * @property string $title
 *
 * @property Thumb[] $thumbs
 */
class Img extends \yii\db\ActiveRecord
{

    public $image;


    public function init()
    {
        parent::init();
        Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/uploads/';
        Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/uploads/';
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_img';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png', 'maxFiles'=>20],

            [['name'], 'required'],
            [['alt', 'title'], 'string'],
            [['name', 'src'], 'string', 'max' => 1023],
            [['ext'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ext' => 'Ext',
            'src' => 'Src',
            'alt' => 'Alt',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbs()
    {
        return $this->hasMany(Thumb::className(), ['img_id' => 'id']);
    }


    /**
     * fetch stored image file name with complete path
     * @return string
     */
    public function getImageFile()
    {
        return isset($this->name) ? Yii::$app->params['uploadPath'] . $this->name : null;
    }


    /**
     * fetch stored image url
     * @return string
     */
    public function getImageUrl()
    {
        // return a default image placeholder if your source avatar is not found
        $name = isset($this->name) ? $this->name : 'not_found.jpg';
        return Yii::$app->params['uploadUrl'] . $name;
    }


    /**
    * Process upload of image
    *
    * @return mixed the uploaded image instance
    */
    public function uploadImage() {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        $image = UploadedFile::getInstance($this, 'image');

        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }

        // store the source file name
        $this->name = Yii::$app->security->generateRandomString() .'-'. $image->name;

        // the uploaded image instance
        return $image;
    }

    // /**
    // * Process upload of image
    // *
    // * @return mixed the uploaded image instance
    // */
    // public function uploadImage() {
    //     // get the uploaded file instance. for multiple file uploads
    //     // the following data will return an array (you may need to use
    //     // getInstances method)
    //     $image = UploadedFile::getInstances($this, 'image');

    //     // if no image was uploaded abort the upload
    //     if (empty($image)) {
    //         return false;
    //     }

    //     // store the source file name
    //     $this->name = Yii::$app->security->generateRandomString() .'-'. $image->name;

    //     // the uploaded image instance
    //     return $image;
    // }



    /**
    * Process deletion of image
    *
    * @return boolean the status of deletion
    */
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->avatar = null;
        $this->filename = null;

        return true;
    }


    public function afterSave()
    {

        return true;
    }
}
