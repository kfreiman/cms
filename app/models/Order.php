<?php

namespace app\models;

use Yii;
use app\models\Item;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "cms_order".
 *
 * @property integer $id
 * @property string $create_time
 * @property string $update_time
 * @property integer $status
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property integer $mode_delivery
 * @property integer $mode_payment
 * @property string $user_comment
 * @property string $admin_comment
 *
 * @property Item[] $items
 */
class Order extends \yii\db\ActiveRecord
{
    private $_crateItemsFromCart = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cms_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'required'],
            [['email'], 'email'],
            // [['phone'], 'safe'],
            [['status', 'mode_delivery', 'mode_payment'], 'integer'],
            [['address', 'user_comment', 'admin_comment'], 'string'],
            [['name', 'phone', 'email'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_time' => 'Время создания',
            'update_time' => 'Время обновления',
            'status' => 'Статус',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'address' => 'Адрес',
            'mode_delivery' => 'Способ доставки',
            'mode_payment' => 'Способ оплаты',
            'user_comment' => 'Комментарий',
            'admin_comment' => 'Комментарий Админа',
            'totalCost' => 'Сумма заказа',
            'totalCount' => 'Кол-во продуктов',
            'count' => 'Кол-во позиций',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['order_id' => 'id']);
    }


    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->create_time = date('Y-m-d H:i:s');
            $this->_crateItemsFromCart = true;
        }
        $this->update_time = date('Y-m-d H:i:s');

        return true;
    }

    public function afterSave()
    {
        if ($this->_crateItemsFromCart) {
            foreach ( \Yii::$app->cart->getPositions() as $position ) {
                $item = new Item();
                $item->order_id = $this->id;
                $item->product_id = $position->id;
                $item->count = $position->getQuantity();
                $item->save();
            }
            \Yii::$app->cart->removeAll();
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsProvider()
    {

        return new ActiveDataProvider([
            'query' => Item::find()->where(['order_id'=>$this->id]),
        ]);
    }

    public function getTotalCost()
    {
        $sum = 0;
        foreach ($this->items as $item) {
            $sum += $item->cost;
        }
        return $sum;
    }

    public function getTotalCount()
    {
        $count = 0;
        foreach ($this->items as $item) {
            $count += $item->count;
        }
        return $count;
    }

    public function getCount()
    {
        return count($this->items);
    }
}
