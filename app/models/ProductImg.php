<?php
namespace app\models;

use Yii;
use app\models\File;

/**
*
*/
class ProductImg extends File
{

    public function behaviors()
    {
        return [
            'imgBehavior' => [
                'class' => \app\components\ImgBehavior::className(),
                'width' => Yii::$app->settings->get('products.width'),
                'height' => Yii::$app->settings->get('products.height'),
                'defaultThumbPath' => Yii::$app->urlManager->baseUrl .'/images/default_group.png',
            ]
        ];
    }


}
