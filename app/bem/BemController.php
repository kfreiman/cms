<?php


namespace app\bem;


use yii\base\ViewContextInterface;
use yii\web\Controller;
use Yii;

use app\assets\Theme1Asset;

/**
*
*/
class BemController extends Controller implements ViewContextInterface
{

    private $_is_client_script = false;

    public function renderBemjson($view, $options=[])
    {
        $this->_is_client_script = true;

        $this->setRenderer();

        // $this->registerTheme();

        $bemjson = $this->renderPartial($view, $options);

        return $this->renderContent($bemjson);
    }


    public function registerTheme()
    {
        $view = $this->getView();
        Theme1Asset::register($view);
    }


    protected function setRenderer()
    {
        Yii::$app->set('view', [
            'class' => 'yii\web\View',
            'renderers' => [
                'handlebars' => [
                    'class' => 'kfreiman\lightncandy\ViewRenderer',
                    'extension' => ['.handlebars','.mustache'],
                ],
            ]
        ]);
    }

    public function getViewPath()
    {
        if ($this->_is_client_script) {
            return Yii::getAlias('@app/client' . DIRECTORY_SEPARATOR . $this->id);
        } else {
            return Yii::getAlias($this->module->getViewPath() . DIRECTORY_SEPARATOR . $this->id);
        }
    }
}

