<?php
use yii\helpers\Url;

return function ($bh, $block_name='navigation') {

    $bh->match($block_name, function($ctx, $json) {
        $json->val = [2];


        if($json->content) {

            $iterateItems = function (&$content) {
                foreach ($content as $key => $item) {
                    if (!$item) {
                        break;
                    }

                    if ($item->content) {
                        // если содержит ссылку на текущую страницу, то авктивна
                        foreach ($item->content as $inner_content) {
                            if ($_SERVER['REQUEST_URI'] == $inner_content->url) {
                                $item->mods->active = true;
                            }
                        }
                    }


                }
            };
            if(is_array($json->content)) throw new \Exception('menu: content must be an array of the menu items');

            $iterateItems($json->content);
        }


    });

};
