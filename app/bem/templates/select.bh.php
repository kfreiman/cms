<?php
return function ($bh) {
    $bh->match('select', function($ctx, $json) {

        $ctx->tag('select');
        // $content = '';

        $refs = new StdClass();
        // $refs->firstOption = null;
        // $refs->checkedOptions = [];
        $containsVal = function ($val) use ($isValDef, $isModeCheck, $json) {
            return $isValDef &&
                ($isModeCheck?
                    in_array($val, $json->val) :
                    $json->val === $val);


        $iterateOptions = function (&$content) use ($containsVal, &$iterateOptions, $refs) {
            foreach ($content as $_ => $option) {
                // if(@$option['group']) {
                //     $iterateOptions(@$content[$_]['group']);
                // } else {
                    $refs->firstOption || ($refs->firstOption =& $content[$_]);
                    // var_dump(compact('refs') + ['contains?' => $containsVal(@$option['val'])]);
                    if($containsVal(@$option['val'])) {
                        $content[$_]['checked'] = true;
                        $refs->checkedOptions[] =& $content[$_];
                    }
                // }
            }
        };


        $iterateOptions($json->options);

        $ctx->content = $content;

        // $url = !$ctx->isSimple($json->url)? // url could contain bemjson
        //     $bh->apply($json->url) :
        //     $json->url;
        // $attrs = [];
        // $tabIndex = null;
        // if (!$ctx->mod('disabled')) {
        //     if($url) {
        //         $attrs['href'] = $url;
        //         $tabIndex = $json->tabIndex;
        //     } else {
        //         $tabIndex = $json->tabIndex ?: 0;
        //     }
        //     $ctx->js(true);
        // } else {
        //     $ctx->js($url? [ 'url' => $url ] : true);
        // }
        // ($tabIndex === null) || ($attrs['tabindex'] = $tabIndex);
        // $json->title && ($attrs['title'] = $json->title);
        // $json->target && ($attrs['target'] = $json->target);
        // $ctx->attrs($attrs);
    //     if (!$ctx->mod('mode')) throw new \Exception('Can\'t build select without mode modifier');
    //     $isValDef = key_exists('val', $json);
    //     $isModeCheck = $ctx->mod('mode') === 'check';
    //     // php!yolo
    //     $refs = new StdClass();
    //     $refs->firstOption = null;
    //     $refs->checkedOptions = [];
    //     $containsVal = function ($val) use ($isValDef, $isModeCheck, $json) {
    //         return $isValDef &&
    //             ($isModeCheck?
    //                 in_array($val, $json->val) :
    //                 $json->val === $val);
    //     };
    //     $iterateOptions = function (&$content) use ($containsVal, &$iterateOptions, $refs) {
    //         foreach ($content as $_ => $option) {
    //             if(@$option['group']) {
    //                 $iterateOptions(@$content[$_]['group']);
    //             } else {
    //                 $refs->firstOption || ($refs->firstOption =& $content[$_]);
    //                 //var_dump(compact('refs') + ['contains?' => $containsVal(@$option['val'])]);
    //                 if($containsVal(@$option['val'])) {
    //                     $content[$_]['checked'] = true;
    //                     $refs->checkedOptions[] =& $content[$_];
    //                 }
    //             }
    //         }
    //     };
    //     $iterateOptions($json->options);
    //     $ctx
    //         ->js([
    //             'name' => $json->name,
    //             'optionsMaxHeight' => $json->optionsMaxHeight
    //         ])
    //         ->tParam('select', $json)
    //         ->tParam('refs', $refs)
    //         ->tParam('firstOption', $refs->firstOption)
    //         ->tParam('checkedOptions', $refs->checkedOptions)
    //         ->content([
    //             [ 'elem' => 'button' ],
    //             [
    //                 'block' => 'popup',
    //                 'mods' => [ 'target' => 'anchor', 'theme' => $ctx->mod('theme'), 'autoclosable' => true ],
    //                 'directions' => ['bottom-left', 'bottom-right', 'top-left', 'top-right'],
    //                 'content' => [ 'block' => $json->block, 'mods' => $ctx->mods(), 'elem' => 'menu' ]
    //             ]
    //         ]);
    // });



    // $bh->match('select_mode_radio', function($ctx) {
    //     $ctx->applyBase();
    //     $refs = $ctx->tParam('refs');
    //     if ($refs->firstOption && empty($refs->checkedOptions)) {
    //         $refs->firstOption['checked'] = true;
    //         $refs->checkedOptions[] = $refs->firstOption;
    //     }
    //     $refs->checkedOption = $refs->checkedOptions[0];
    //     $ctx
    //         ->tParam('checkedOption', $refs->checkedOption)
    //         ->content([
    //             [
    //                 'elem' => 'control',
    //                 'val' => @$refs->checkedOption['val']
    //             ],
    //             $ctx->content()
    //         ], true);
    // });
    // $bh->match('select_mode_radio__button', function($ctx) {
    //     $ctx->content([
    //         'elem' => 'text',
    //         'content' => @$ctx->tParam('refs')->checkedOption['text']
    //     ]);
    });
};
