<?php
// use yii\helpers\Url;

return function ($bh, $block_name='cart-add-modal') {

    $bh->match($block_name, function($ctx, $json) {
        $json->attrs['role'] = 'dialog';
        $ctx->cls(' modal fade ');
        // var_dump($json->buttons['dismiss']);
        $ctx->content([
            [
                'elem' => 'modal-dialog modal-md',
                'cls' => 'modal-dialog modal-md',
                'content' => [
                    [
                        'elem' => 'content',
                        'cls' => 'modal-content',
                        'content' => [
                            [
                                'elem' => 'header',
                                'cls' => 'modal-header',
                                'content' => $json->header_text ?: '<h3>Продукт добавлен в корзину</h3>'
                            ],

                            $json->body_text ?
                                [
                                    'elem' => 'body',
                                    'cls' => 'modal-body',
                                    'content' => $json->body_text ?: ''
                                ] :
                                null,


                            $json->buttons ? getButtons($ctx, $json) : null,

                        ]
                    ]
                ]
            ],

        ]);



    });

    function getButtons($ctx, $json) {
        return [
            'elem' => 'footer',
            'cls' => 'modal-footer',
            'content' => [
                $json->buttons['dismiss'] ? (
                    $json->buttons['dismiss'] === true ?
                        [
                            'block' => 'to-cart-button',
                            'cls' => 'btn btn-sm btn-default',
                            'attrs' => ['data-dismiss' => 'modal'],
                            'content' => 'Продолжить покупки',
                        ] :
                        $json->buttons['dismiss']
                    ) :
                    null ,

                $json->buttons['to-cart'] ? (
                    $json->buttons['to-cart'] === true ?
                        [
                            'block' => 'to-cart-button',
                            'tag' => 'a',
                            'attrs' => ['href' => '/site/cart'],
                            'cls' => 'btn btn-sm btn-primary',
                            'content' => 'Перейти в корзину',
                        ] :
                        $json->buttons['dismiss']
                    ) :
                    null ,
            ]
        ];
    }
};
