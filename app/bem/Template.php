<?php


namespace app\bem;

use app\bh\templates\Menu;
use app\bh\templates\Page;
use app\bh\templates\NavigationMenu;
/**
*
*/
class Template
{

    public function apply($bemjson)
    {
        $bh = new \BEM\BH();

        // настройки
        $bh->setOptions([
            "jsAttrName" => "data-bem",
            "jsAttrScheme" => "i-bem"
        ]);


        $fn = include 'templates/cart-add-modal.bh.php';
        $fn($bh);

        $fn = include 'templates/link.bh.php';
        $fn($bh);

        $fn = include 'templates/image.bh.php';
        $fn($bh);

        // $fn = include 'templates/select.bh.php';
        // $fn($bh);

        // $fn = include 'templates/Page.php';
        // $fn($bh);


        return $bh->apply($bemjson);
    }
}


?>
